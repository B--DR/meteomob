import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Search from '../components/Search';
import Result from '../components/Result';

const Stack = createStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Météo"
                component={Search}
            />
            <Stack.Screen name="Resultat" component={Result} />
        </Stack.Navigator>
    )
}

export default HomeStack;