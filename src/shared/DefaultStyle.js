import { StyleSheet } from "react-native";
import { Text } from 'react-native';
import Styled from 'styled-components';

export const Direction = Styled(Text)`
    font-size: 45px;
    color: #fff;
    transform: ${({deg})=> (deg && `rotate(${deg}deg);`)};
    transform-orgin: center center;
`;


const defaultStyle = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
      },
    lineView: {
        flex: 1,
        flexDirection: "row",
        alignItems: 'flex-end',
        width: '100%',
    },
    image: {
        position: 'absolute',
        width: '100%',
        height: '100%',

    },
    marginDefault: {
        margin: 20,
        marginTop: 40,
    },
    inputDefault: {
        width: '100%',
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        backgroundColor: "rgba(255,255,255,.3)",
        color: '#fff',
        zIndex: 100,
    },
    buttonDefault: {
        borderRadius: 5,
        borderColor: "#ddd",
        borderWidth: 1,
        marginTop: '40px',

    },
    buttonNext: {
        alignSelf: 'flex-end',
    },
    temp: {
        color: '#fff',
        fontSize: '45px',
        textAlign: 'center',
    },
    description: {
        color: '#fff',
        textAlign: 'center',
        fontSize: '30px',
    },
    ressentTemp: {
        fontSize: '12px',
        color: '#fff',
        textAlign: 'center',
    }
})

export default defaultStyle;