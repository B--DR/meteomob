const apiKeyc = '1c31761bb1984ab797013176ce193191';
const apiKey = '60283637e67c196b4ef78b5383cd493d';
const unitesUrl = 'units=Metric';
const previsionDays = 'cnt=3';
const langUrl = 'lang=fr';

export const urlGetCoordinatesByCity = (name) => `https://api.opencagedata.com/geocode/v1/json?key=${apiKeyc}&q=${name}`;

export const urlWeatherForecast = ({lat, lng}) => `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&${unitesUrl}&${langUrl}&${previsionDays}&appid=${apiKey}`;

export const urlWeatherCity = (city) => `https://api.openweathermap.org/data/2.5/weather?q=${city}&${unitesUrl}&${langUrl}&${previsionDays}&appid=${apiKey}`;
