import { WEATHER } from '../actions/weatherActions';
const initialState = { loading: false };

const myWeatherReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case WEATHER.FETCH.REQUEST:
      return {loading: true};
    case WEATHER.FETCH.SUCCESS:
      return {...payload, loading: false};
    default:
      return state;
  }
};

export default myWeatherReducer;