import { COORD } from '../actions/coordinatesAction';
const initialState = { loading: false };

const myCoordinatesReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case COORD.FETCH.REQUEST:
      return {loading: true};
    case COORD.FETCH.SUCCESS:
      return {loading: false, ...payload};
    default:
      return state;
  }
};

export default myCoordinatesReducer;
