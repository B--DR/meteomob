import { CITYWEATHER } from '../actions/weatherCityAction';
const initialState = { loading: false };

const myCityWeatherReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CITYWEATHER.FETCH.REQUEST:
      return {loading: true};
    case CITYWEATHER.FETCH.SUCCESS:
      return {...payload, loading: false};
    default:
      return state;
  }
};

export default myCityWeatherReducer;