import axios from 'axios';
import { urlGetCoordinatesByCity } from '../utils/urls';

export const getName = async (name) => {
  const url = urlGetCoordinatesByCity(name);

  return  await axios.get(
    url,
  ).then((res) => res.data.results[0].geometry);
};

export default getName;
