import axios from 'axios';
import { urlWeatherForecast } from '../utils/urls';

export const getWeather = async (lat, lng) => {
    const url = urlWeatherForecast(lat, lng);

    return await axios.get(url)
    .then((res) => res.data)
};

export default getWeather;
