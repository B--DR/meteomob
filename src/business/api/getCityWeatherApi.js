import axios from 'axios';
import { urlWeatherCity } from '../utils/urls';

export const getCityWeather = async (city) => {
    const url = urlWeatherCity(city);

    return await axios.get(url)
    .then((res) => res.data)
};

export default getCityWeather;
