import action from '../utils/action';
import createRequestTypes from '../utils/createRequestTypes';

const FETCH = 'FETCH';

export const CITYWEATHER = createRequestTypes('CITYWEATHER', [
    FETCH,
])

export const getCityWeather = {
    request: (city) => action(CITYWEATHER.FETCH.REQUEST, city),
    success: (cityWeather) => action(CITYWEATHER.FETCH.SUCCESS, cityWeather),
    failure: (error) => action(CITYWEATHER.FETCH.FAILURE, { error })
};
