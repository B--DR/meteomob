import action from '../utils/action';
import createRequestTypes from '../utils/createRequestTypes';

const FETCH = 'FETCH';

export const COORD = createRequestTypes('COORD', [
  FETCH,
]);

export const getCityCoordinates = {
  request: (city) => action(COORD.FETCH.REQUEST, city),
  success: (coordinates) => action(COORD.FETCH.SUCCESS, coordinates),
  failure: (error) => action(COORD.FETCH.FAILURE, { error }),
};
