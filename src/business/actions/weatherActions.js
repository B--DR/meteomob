import action from '../utils/action';
import createRequestTypes from '../utils/createRequestTypes';

const FETCH = 'FETCH';

export const WEATHER = createRequestTypes('WEATHER', [
    FETCH,
])

export const getWeather = {
    request: (coord) => action(WEATHER.FETCH.REQUEST, coord),
    success: (datas) => action(WEATHER.FETCH.SUCCESS, datas),
    failure: (error) => action(WEATHER.FETCH.FAILURE, { error })
};
