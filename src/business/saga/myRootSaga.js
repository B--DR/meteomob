import { all } from 'redux-saga/effects';
import { myCoordinatesSaga, myWeatherSaga,myCityWeatherSaga } from './weatherSaga';

export default function* getSagaRoot() {
  yield all([
    myCoordinatesSaga(),
    myWeatherSaga(),
    myCityWeatherSaga(),
  ]);
}
