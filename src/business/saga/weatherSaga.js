import {
    takeEvery, call, put,
  } from 'redux-saga/effects';
  
  import {
    COORD,
    getCityCoordinates as getCoordinatesAction,
  } from '../actions/coordinatesAction';
  
  import { getName } from '../api/getCoordinatesApi';

  import {
    WEATHER,
    getWeather as getWeatherAction,
  } from'../actions/weatherActions';

  import { getWeather } from '../api/getWeatherApi';

  import {
    CITYWEATHER,
    getCityWeather as getCityWeatherAction,
  } from'../actions/weatherCityAction';

  import { getCityWeather } from '../api/getCityWeatherApi';
  
  
  export function* getCoordinatesByName({ payload }) {
    try {
      const myCoordinates = yield call(getName, payload);
  
      yield put(getCoordinatesAction.success(myCoordinates));
    } catch (error) {
      yield put(getCoodrinatesAction.failure(error));
    }
  }
  
  export function* myCoordinatesSaga() {
    yield takeEvery(COORD.FETCH.REQUEST, getCoordinatesByName);
  }

  export function* getWeatherByCoord({ payload }) {
    try {
      const myWeather = yield call(getWeather, payload);
      yield put(getWeatherAction.success(myWeather));
    } catch (error) {
      yield put(getWeatherAction.failure(error));
    }
  }

  export function* myWeatherSaga() {
    yield takeEvery(WEATHER.FETCH.REQUEST, getWeatherByCoord);
  }

  export function* getWeatherByCity({ payload }) {
    try {
      const myCityWeather = yield call(getCityWeather, payload);
      yield put(getCityWeatherAction.success(myCityWeather));
    } catch (error) {
      yield put(getCityWeatherAction.failure(error));
    }
  }

  export function* myCityWeatherSaga() {
    yield takeEvery(CITYWEATHER.FETCH.REQUEST, getWeatherByCity);
  }
  