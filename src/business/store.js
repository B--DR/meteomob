import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
} from 'redux';
import { persistStore } from 'redux-persist';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';

import getSagaRoot from './saga/myRootSaga';
import myCoordinatesReducer from './reducer/coordinatesReducer';
import myWeatherReducer from './reducer/weatherReducer';
import myCityWeatherReducer from './reducer/weatherCityReducer';

const rootReducer = combineReducers({
  coordinates: myCoordinatesReducer,
  weather: myWeatherReducer,
  cityWeather: myCityWeatherReducer,
});

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const sagaMiddleware = createSagaMiddleware();
const logger = createLogger();

export const store = createStore(
  rootReducer,
  composeEnhancer(applyMiddleware(sagaMiddleware, logger)),
);

sagaMiddleware.run(getSagaRoot);

export const persistor = persistStore(store);
