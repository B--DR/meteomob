import * as React from 'react';
import { Text } from 'react-native';

const ResultTitle = ({ city }) => {
    return (
        <Text>{city}</Text>
    )
}

export default ResultTitle;