import * as React from 'react';
import { Text, View, Button } from 'react-native';
import ResultTitle from './ResultTitle';
import { useDispatch, useSelector } from 'react-redux';

import { getWeather } from '../business/actions/weatherActions';
// import getApiWeather from './api/GetApiWeather';

const Result = ({route}) => {
    const city = route.params.city;
    const [datas, setDatas] = React.useState();
    const coordinates = useSelector((state)=> state.coordinates);
   
    console.log("Result -> daily", daily)
    const { lat, lng } = coordinates;
    const weather = useSelector((state) => state.weather);
    const { daily, current } = weather;
    
    // const test = getApiWeather(city);
    // console.log("Result -> test", test)
    const dispatch = useDispatch();
    
    React.useEffect(() => {
        dispatch(getWeather.request({lat, lng}));
    },[])
    
    const currentWeather = () => {
        if(current) {
            const { description } = current?.weather[0];
            return (
                <Text>aujourd'hui : {description}</Text>
            )
        }
    }

    const dailyWeather = () => {
        return daily?.map((day)=>{
            const {description} = day.weather[0];
            return (
                <View key={day.dt}>
                    <Text>{description}</Text>
                    <Text>température : {day.temp.day}°C</Text>
                </View>
            )
        })
        
    }
    
    return (
        <View>
            <Text>les résultats de </Text>
            <ResultTitle city={city} />
                {dailyWeather()}
                {currentWeather()}
        </View>
        
    )
}

export default Result;