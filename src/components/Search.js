import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCityCoordinates } from '../business/actions/coordinatesAction';
import { getCityWeather } from '../business/actions/weatherCityAction';
import Icon from 'react-native-vector-icons/Fontisto';

import { View, Text, TextInput, Button, ActivityIndicator, Image, SegmentedControlIOSComponent } from 'react-native';
import defaultStyle, { Direction } from '../shared/DefaultStyle';
import { itemPictos } from '../shared/ItemPictos';

const Search = ({ navigation }) => {
    const [cityName, setCityName] = React.useState('Bordeaux');
    
    const { main, wind, weather, loadingCity } = useSelector((state) => ({
        main: state.cityWeather.main,
        wind: state.cityWeather.wind,
        weather: state.cityWeather.weather,
        loadingCity: state.cityWeather.loading,
    }));
   const reduceTemp = (value) => {
        return Math.round(value);
   }

    const coordinates = useSelector((state)=> state.coordinates);
    
    const { loading } = coordinates;
    const { loading: cityLoading } = loadingCity;
    const dispatch = useDispatch();
    
    const searchCity = (value) => {
        dispatch(getCityCoordinates.request(value));
        setCityName(value);
        setTimeout(() => {
            navigation.navigate('Resultat', {city: cityName});
        }, 800)
    }
    
    const handleChangeInputVal = (val) => {
        console.log("handleChangeInputVal -> val", val)
        
        if(val !== ' '){
            const newVal = val.replace(/ /g, '-');
            setCityName(newVal);
        }else{
            const valWithoutSpace = val.replace(/ /g, '');
            setCityName(valWithoutSpace);
        }
    }
    
    React.useEffect(() => {
        getDatasWeather();
    }, [])

    const getDatasWeather = () => {
        dispatch(getCityWeather.request(cityName));
    }

    
    return (
        <View style={defaultStyle.container}>
             <Image
                source={require('../assets/images/bg-home.jpg')}
                style={defaultStyle.image}
                resizeMode='cover'
            />
            <View>
                <TextInput
                    style={defaultStyle.inputDefault}
                    underlineColorAndroid="transparent"
                    placeholder="indiquer votre ville"
                    onChangeText={(text) => handleChangeInputVal(text)}
                    value={cityName}
                />
                <Button
                    title={`météo à ${cityName}`}
                    style={defaultStyle.buttonDefault}
                    onPress={() => getDatasWeather()}
                />
            </View>
            
           
            {loading || cityLoading && (
                <ActivityIndicator />
            )}
            {main && (
                <View>
                    <Text style={defaultStyle.temp}>{reduceTemp(main.temp)}°C</Text>
                    <Text style={defaultStyle.ressentTemp}>ressenti : {reduceTemp(main.feels_like)}°C</Text>
                    <Icon name={itemPictos[weather[0].icon]} size={30} color="#fff" />
                    <Text style={defaultStyle.description}>{weather[0].description}</Text>
                    <Icon name="wind" size={15} color="#fff" />
                    <Text style={defaultStyle.description}>{wind.speed}</Text>
                </View>
            )}
            {weather && (
                <View style={defaultStyle.lineView}>
                    <Button
                        title="voir les prévisions"
                        style={[defaultStyle.buttonDefault, defaultStyle.buttonNext]}
                        onPress={() => {
                            searchCity(cityName);
                            getDatasWeather();
                        }}
                    />
                </View>
            )}
        </View>
    )
}


export default Search;