import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet, Text, View } from 'react-native';
import { store } from './src/business/store';
import About from './src/components/About';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeStack from './src/navigation/StackNavigationHome';

const Tab = createBottomTabNavigator();

const BottomTab = () => {
  return(
    <NavigationContainer>
      <Provider store={store}>
        <Tab.Navigator
          screenOptions={({ route }) => ({
            tabBarIcon: ({ color, size }) => {
              let iconName;
              if (route.name === "Météo") {
                iconName = 'ios-umbrella';
              }
              if (route.name === "Credits") {
                iconName = 'ios-finger-print'
              }
              return <Ionicons name={iconName} size={size} color={color} />
            }
          })}
          tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
          }}>
          <Tab.Screen name="Météo" component={HomeStack} />
          <Tab.Screen name="Credits" component={About} />
        </Tab.Navigator>
      </Provider>
    </NavigationContainer>
  )
}



export default function App() {
  return (
    <>
    <StatusBar hidden={true} />
    <BottomTab />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
